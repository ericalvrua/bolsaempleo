<?php

namespace App\Controller;

use App\Entity\Usuarios;
use App\Entity\PasswordReset;
use App\Form\UsuariosType;
use App\Repository\EmpresasRepository;
use App\Repository\OfertasRepository;
use App\Repository\UsuariosRepository;
use App\Repository\PasswordResetRepository;
use App\Service\Correos;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/usuarios")
 */
class UsuariosController extends AbstractController
{
    /**
     * Funcion que nos permitira logear a un usuario
     * @Route("/login", name="usuarios_login", methods={"GET", "POST"})
     */
    public function usuariosLogin(UsuariosRepository $usuariosRepository, EntityManagerInterface $em, Request $request, SessionInterface $session): Response
    {
        $correo = $request->request->get('correo');
        $pass = $request->request->get('pass');
        $usuario = $usuariosRepository->findOneBy(['correo' => $correo]);

        if ($request->request->get('correo') != "") {
            // Si el correo existe, comprobamos las contraseñas y creamos la sesion del usuario
            if (!empty($usuario)) {
                if (password_verify($pass, $usuario->getPass())) {
                    $session->set('usuario', ['correo' => $usuario->getCorreo(), 'id' => $usuario->getId()]);
                    return $this->redirectToRoute('usuarios_perfil');
                } else {
                    // Si es incorrecta la contraseña, se devuelve a la pagina con un error
                    return $this->render('usuarios/login.html.twig', [
                        'usuarios' => $usuariosRepository->findAll(),
                        'error' => 'Error: contraseña incorrecta',
                    ]);
                }
            } else {
                // Si es incorrecto el correo, se devuelve a la pagina con un error
                return $this->render('usuarios/login.html.twig', [
                    'usuarios' => $usuariosRepository->findAll(),
                    'error' => 'Error: el correo indicado no existe',
                ]);
            }
        }

        return $this->render('usuarios/login.html.twig', [
            'usuarios' => $usuariosRepository->findAll(),
        ]);
    }

    /**
     * Genera el perfil de un usuario
     * @Route("/perfil", name="usuarios_perfil", methods={"GET"})
     */
    public function usuariosPerfil(UsuariosRepository $usuariosRepository, SessionInterface $session): Response
    {
        return $this->render('usuarios/perfil.html.twig', [
            'usuario' => $usuariosRepository->findOneBy(['correo' => $session->get('usuario')['correo']]),
        ]);
    }

    /**
     * Cierra la sesion de un usuario
     * @Route("/cerrarSesion", name="usuarios_cerrar_sesion", methods={"GET"})
     */
    public function usuariosCerrarSesion(UsuariosRepository $usuariosRepository, SessionInterface $session): Response
    {
        $session->remove('usuario'); // Elimina la sesion del usuario
        return $this->render('usuarios/login.html.twig', [
            'usuarios' => $usuariosRepository->findAll(),
        ]);
    }

    /**
     * Funcion que permite la creacion de un usuario.
     * @Route("/crear", name="usuarios_crear", methods={"GET","POST"})
     */
    public function crearUsuario(Request $request, EntityManagerInterface $em, Correos $correos): Response
    {
        $usuario = new Usuarios();
        $nombre = $request->request->get('nombre');
        $identificacion = $request->request->get('identificacion');
        $apellidos = $request->request->get('apellidos');
        $pass = $request->request->get('pass');
        $pass2 = $request->request->get('pass2');
        $correo = $request->request->get('correo');
        $correo2 = $request->request->get('correo2');
        $fecha_nacimiento = $request->request->get('fecha_nacimiento');
        $telefono = $request->request->get('telefono');
        // Comprobacion de que tanto los correos como las contraseñas son iguales
        if ($pass == $pass2 && $correo == $correo2) {
            if (!empty($nombre)) {

                $em = $this->getDoctrine()->getManager();
                $usuario->setNombre($nombre);
                $usuario->setDni($identificacion);
                $usuario->setApellidos($apellidos);
                $usuario->setPass(password_hash($pass, PASSWORD_DEFAULT));
                $usuario->setCorreo($correo);
                // Colocamos la fecha de nacimiento de la siguiente manera
                $usuario->setFechaNacimiento(\DateTime::createFromFormat('Y-m-d', $fecha_nacimiento));
                $usuario->setTelefono($telefono);
                $em->persist($usuario);
                $em->flush();
                $correos->correoCrearCuenta($correo);
                return $this->redirectToRoute('usuarios_crear');
            }
        }
        return $this->render('usuarios/crear.html.twig', [
            'usuario' => $usuario
        ]);
    }

    /**
     * Esta funcion nos permite editar la informacion de un usuario.
     * @Route("/editar", name="usuarios_editar", methods={"GET","POST"})
     */
    public function editar(Request $request, UsuariosRepository $usuariosRepository, SessionInterface $session, SluggerInterface $slugger): Response
    {
        $usuario = $usuariosRepository->findOneBy(['id' => $session->get('usuario')['id']]);
        if ($request->get('nombre')) {
            $nombre = $request->get('nombre');
            $apellidos = $request->get('apellidos');
            $telefono = $request->get('telefono');
            if (!empty($nombre) && !empty($apellidos) && !empty($telefono)) {
                $em = $this->getDoctrine()->getManager();
                $usuario->setNombre($nombre);
                $usuario->setApellidos($apellidos);
                $usuario->setTelefono($telefono);
                // Si en el request no esta vacio, hacemos set al valor, en caso contrario no hacemos nada
                !empty($request->get('direccion')) ? $usuario->setDireccion($request->get('direccion')) : null;
                !empty($request->get('codigo_postal')) ? $usuario->setCodigoPostal($request->get('codigo_postal')) : null;
                !empty($request->get('pais')) ? $usuario->setPais($request->get('pais')) : null;
                !empty($request->get('provincia')) ? $usuario->setProvincia($request->get('provincia')) : null;
                !empty($request->get('localidad')) ? $usuario->setLocalidad($request->get('localidad')) : null;
                !empty($request->get('descripcion')) ? $usuario->setDescripcion($request->get('descripcion')) : null;

                //Subir CV
                if (!empty($request->files->get('cv'))) {
                    // Recogemos el fichero y tambien le creamos un nuevo nombre de forma segura
                    $archivo = $request->files->get('cv');
                    $originalFilename = pathinfo($archivo->getClientOriginalName(), PATHINFO_FILENAME);
                    $safeFilename = $slugger->slug($originalFilename);
                    $newFilename = $safeFilename . '-' . uniqid() . '.' . $archivo->guessExtension();

                    // Movemos el fichero a nuestra carpeta publica
                    $archivo->move('uploads/cv', $newFilename);

                    $usuario->setCv($newFilename);
                }

                $em->persist($usuario);
                $em->flush();
                return $this->redirectToRoute('usuarios_perfil');
            }
        }


        return $this->render('usuarios/editar.html.twig', [
            'usuario' => $usuario,
        ]);
    }


    /**
     * Esta funcion mandara un correo al usuario con un enlace que permitira cambiar la contraseña.
     * @Route("/mailPass", name="usuarios_mail_pass", methods={"GET","POST"})
     */
    public function mailPass(Request $request, UsuariosRepository $usuariosRepository, SessionInterface $session, PasswordResetRepository $passReset, Correos $correos, EntityManagerInterface $em, MailerInterface $mailer): Response
    {
        
        if ($request->get('correo') != "") {
            // Comprobamos que el correo exista en la base de datos
            if ($usuariosRepository->findOneBy(['correo' => $request->get('correo')]) == "" ) {
                return $this->render('usuarios/mailPass.html.twig', [
                    'error' => 'No hay ningun usuario con este correo electronico'
                ]);
            }

            $email = $request->get('correo');
            // Si ya se habia generado un token para ese correo, se borra y genera otro
            if ($passReset->findOneBy(['email' => $email]) != "") {
                $em->remove($passReset->findOneBy(['email' => $email]));
                $em->flush();
            }

            // Generamos un nuevo token para cambiar la contraseña
            $passReset = new PasswordReset();
            $token = md5(random_bytes(40));
            $passReset->setToken($token);

            $passReset->setEmail($email);
            $em = $this->getDoctrine()->getManager();
            $em->persist($passReset);
            $em->flush();
            $url = $this->generateUrl('usuarios_cambiar_pass', [], UrlGeneratorInterface::ABSOLUTE_URL);
            $url = $url."?token=$token";
            $mensaje = 'Vaya aqui para cambiar la contrase&ntilde;a <a href="'.$url.'">Enlace</a>';
            
            $mail = (new Email())
            ->from('proyectoskeleton@gmail.com')
            ->to($email)
            ->subject('Restaurar contraseña')
            ->html("$mensaje");

            $mailer->send($mail);

            return $this->render('usuarios/correoCambiarPass.html.twig');
        }

        return $this->render('usuarios/mailPass.html.twig');
    }

    /**
     * Esta funcion nos permite cambiar la contraseña del usuario
     * @Route("/cambiarPass", name="usuarios_cambiar_pass", methods={"GET","POST"})
     */
    public function cambiarPass(Request $request, UsuariosRepository $usuariosRepository, SessionInterface $session, PasswordResetRepository $passReset, Correos $correos, EntityManagerInterface $em): Response
    {
        // Si el token no existe, se redirigira al inicio de la web
        if ($passReset->findOneBy(['token' => $request->get('token')]) == "") {
            return $this->redirectToRoute('index');
        }

        if ($request->get('pass') != "") {
            $token = $passReset->findOneBy(['token' => $request->get('token')]);
            $usuario = $usuariosRepository->findOneBy(['correo' => $token->getEmail()]);

            $usuario->setPass(password_hash($request->get('pass'), PASSWORD_DEFAULT));
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->remove($token);
            $em->flush();
            return $this->redirectToRoute('usuarios_login');

        }

        return $this->render('usuarios/changePass.html.twig', [
            'token' => $request->query->get('token')
        ]);
    }


    /**
     * Esta funcion nos permite ver la informacion de un usuario.
     * @Route("/{id}", name="usuarios_show", methods={"GET"})
     */
    public function show(Usuarios $usuario): Response
    {
        return $this->render('usuarios/show.html.twig', [
            'usuario' => $usuario,
        ]);
    }

    /**
     * Esta funcion permitira mandar a las empresas un correo al usuario.
     * @Route("/crearCorreo/{idOferta}/{idUsuario}", name="usuarios_crearCorreo", methods={"GET", "POST"})
     */
    public function crearCorreo($idUsuario, $idOferta, Request $request, MailerInterface $mailer, SessionInterface $session, EmpresasRepository $empresasRepository, OfertasRepository $ofertasRepository, UsuariosRepository $usuariosRepository, Correos $correos): Response
    {
        if ($request->get('asunto') != "") {
            
            $oferta = $ofertasRepository->findOneBy(['id' => $idOferta]);
            $empresa = $empresasRepository->findOneBy(['id' => $session->get('empresa')['id']]);
            // Comprobamos que la oferta es de la empresa que se encuentra en sesion
            if ($empresa != $oferta->getIdEmpresa()) {
                return $this->redirectToRoute('index');
            }

            $correoUsuario = $usuariosRepository->findOneBy(['id' => $idUsuario])->getCorreo();
            // Enviar correo
            $correos->crearCorreo($correoUsuario, $empresa, $oferta, $request->get('asunto'), $request->get('mensaje'), true, true);

        return $this->redirectToRoute('empresas_perfil');
        }

        return $this->render('usuarios/correo.html.twig', [
            'id' => $idUsuario,
            'oferta' => $idOferta
        ]);
    }

}
        