function estilo() {
    let theme = document.getElementById("theme");
    let icono = document.getElementById("tema");

    if (theme.getAttribute("href") == "/css/empleo-noche.css") {
      theme.href = "/css/empleo-dia.css";
      icono.src = "/css/luna.png";
      sessionStorage.setItem("luz", "true");
    } else {
      theme.href = "/css/empleo-noche.css";
      icono.src = "/css/sol.png";
      sessionStorage.setItem("luz", "false");
    }
  }