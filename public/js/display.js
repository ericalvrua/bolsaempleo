function mostrar() {
    let mostrarCrearCarpeta = document.getElementById("otros");
    let boton = document.getElementById("masfiltros");

    if (mostrarCrearCarpeta.style.display == 'none' || mostrarCrearCarpeta.style.display == '') {
        mostrarCrearCarpeta.style.display = 'block';
        boton.innerText = "Mostrar menos filtros";
    } else {
        mostrarCrearCarpeta.style.display = 'none';
        boton.innerText = "Mostrar más filtros";
    }
}